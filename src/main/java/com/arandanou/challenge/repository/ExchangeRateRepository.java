package com.arandanou.challenge.repository;

import com.arandanou.challenge.model.entity.ExchangeRateEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExchangeRateRepository extends JpaRepository<ExchangeRateEntity, Integer> {

    ExchangeRateEntity findByOriginCurrencyAndDestinationCurrency(
            String originCurrency, String destinationCurrency);


}
