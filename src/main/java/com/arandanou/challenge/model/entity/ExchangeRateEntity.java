package com.arandanou.challenge.model.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Builder
@Entity
@Table(name = "TBL_EXCHANGE_RATE")
@NoArgsConstructor
@AllArgsConstructor
public class ExchangeRateEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BIE_E_DOCUMENT_ROSTER_GEN")
    @Column(name = "EXCHANGE_RATE_ID")
    private Integer exchangeRateId;

    @Column(name = "ORIGIN_CURRENCY")
    private String originCurrency;

    @Column(name = "DESTINATION_CURRENCY")
    private String destinationCurrency;

    @Column(name = "VALUE")
    private BigDecimal value;

}

