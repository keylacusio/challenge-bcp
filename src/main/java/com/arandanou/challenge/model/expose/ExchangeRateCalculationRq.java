package com.arandanou.challenge.model.expose;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExchangeRateCalculationRq {
    private String originCurrency;
    private String destinationCurrency;
    private BigDecimal amount;

}
