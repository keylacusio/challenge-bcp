package com.arandanou.challenge.model.expose;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class LoginRs {
    private String token;
}
