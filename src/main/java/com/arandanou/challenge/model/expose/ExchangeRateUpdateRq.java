package com.arandanou.challenge.model.expose;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExchangeRateUpdateRq {
    private Integer exchangeRateId;
    private String originCurrency;
    private String destinationCurrency;
    private BigDecimal value;
}
