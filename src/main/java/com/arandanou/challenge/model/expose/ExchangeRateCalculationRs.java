package com.arandanou.challenge.model.expose;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExchangeRateCalculationRs {
    private String originCurrency;
    private String destinationCurrency;
    private BigDecimal amount;
    private BigDecimal amountChanged;
}
