package com.arandanou.challenge.model.expose;

import lombok.Data;

@Data
public class LoginRq {
    private String email;
    private String password;
}
