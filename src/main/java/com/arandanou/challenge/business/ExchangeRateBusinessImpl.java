package com.arandanou.challenge.business;

import com.arandanou.challenge.business.impl.ExchangeRateBusiness;
import com.arandanou.challenge.model.entity.ExchangeRateEntity;
import com.arandanou.challenge.model.expose.ExchangeRateCalculationRq;
import com.arandanou.challenge.model.expose.ExchangeRateCalculationRs;
import com.arandanou.challenge.model.expose.ExchangeRateRq;
import com.arandanou.challenge.model.expose.ExchangeRateUpdateRq;
import com.arandanou.challenge.repository.ExchangeRateRepository;
import io.reactivex.Observable;
import io.reactivex.Single;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;

@Service
public class ExchangeRateBusinessImpl implements ExchangeRateBusiness {

    @Autowired
    private ExchangeRateRepository exchangeRateRepository;


    @Override
    public Single<ExchangeRateEntity> save(ExchangeRateRq request) {
        return Single.just(exchangeRateRepository.save(mapSave.apply(request)));
    }

    @Override
    public Single<ExchangeRateEntity> update(ExchangeRateUpdateRq request) {
        return Single.just(exchangeRateRepository.save(mapUpdate.apply(request)));
    }

    @Override
    public Observable<ExchangeRateEntity> exchangeRateList() {
        return Observable.fromIterable(exchangeRateRepository.findAll());
    }

    @Override
    public Observable<ExchangeRateCalculationRs> calculate(ExchangeRateCalculationRq request) {

        return Observable.just(request)
                //consultar los tipos de cambio de la db temporal
                .flatMap(req -> Objects.isNull(exchangeRateRepository.findByOriginCurrencyAndDestinationCurrency(
                        req.getOriginCurrency(), req.getDestinationCurrency())) ? Observable.empty() : Observable.just(
                        exchangeRateRepository.findByOriginCurrencyAndDestinationCurrency(
                                req.getOriginCurrency(), req.getDestinationCurrency()))
                        .switchIfEmpty(Observable.error(new Throwable("Error")))
                        .take(1)
                        .flatMap(entity -> calculateValue.apply(req.getAmount(), entity)));

    }

    private BiFunction<BigDecimal, ExchangeRateEntity, Observable<ExchangeRateCalculationRs>> calculateValue =
            (amount, entity) -> Observable.just(ExchangeRateCalculationRs.builder()
                    .amount(amount)
                    .amountChanged(amount.multiply(entity.getValue()))
                    .destinationCurrency(entity.getDestinationCurrency())
                    .originCurrency(entity.getOriginCurrency())
                    .build());

    private Function<ExchangeRateRq, ExchangeRateEntity> mapSave =
            request -> ExchangeRateEntity.builder()
                    .originCurrency(request.getOriginCurrency())
                    .destinationCurrency(request.getDestinationCurrency())
                    .value(request.getValue())
                    .build();

    private Function<ExchangeRateUpdateRq, ExchangeRateEntity> mapUpdate =
            request -> ExchangeRateEntity.builder()
                    .exchangeRateId(request.getExchangeRateId())
                    .originCurrency(request.getOriginCurrency())
                    .destinationCurrency(request.getDestinationCurrency())
                    .value(request.getValue())
                    .build();

}
