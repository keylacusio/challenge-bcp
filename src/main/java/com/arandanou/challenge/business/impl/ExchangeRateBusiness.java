package com.arandanou.challenge.business.impl;

import com.arandanou.challenge.model.entity.ExchangeRateEntity;
import com.arandanou.challenge.model.expose.ExchangeRateCalculationRq;
import com.arandanou.challenge.model.expose.ExchangeRateCalculationRs;
import com.arandanou.challenge.model.expose.ExchangeRateRq;

import com.arandanou.challenge.model.expose.ExchangeRateUpdateRq;
import io.reactivex.Observable;
import io.reactivex.Single;

public interface ExchangeRateBusiness {

    Single<ExchangeRateEntity> save(ExchangeRateRq request);

    Single<ExchangeRateEntity> update(ExchangeRateUpdateRq request);

    Observable<ExchangeRateEntity> exchangeRateList();

    Observable<ExchangeRateCalculationRs> calculate(ExchangeRateCalculationRq request);


}
