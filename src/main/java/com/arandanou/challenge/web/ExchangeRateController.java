package com.arandanou.challenge.web;

import com.arandanou.challenge.business.impl.ExchangeRateBusiness;
import com.arandanou.challenge.model.entity.ExchangeRateEntity;
import com.arandanou.challenge.model.expose.ExchangeRateCalculationRq;
import com.arandanou.challenge.model.expose.ExchangeRateCalculationRs;
import com.arandanou.challenge.model.expose.ExchangeRateRq;
import com.arandanou.challenge.model.expose.ExchangeRateUpdateRq;
import io.reactivex.Observable;
import io.reactivex.Single;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RequestMapping(value="/exchange-rate")
@RequiredArgsConstructor
@RestController
public class ExchangeRateController {

    private final ExchangeRateBusiness exchangeRateBusiness;

    @PostMapping
    public Single<ExchangeRateEntity> register(@RequestBody ExchangeRateRq request) {
        return exchangeRateBusiness.save(request);
    }

    @PutMapping
    public Single<ExchangeRateEntity> update(@RequestBody ExchangeRateUpdateRq request) {
        return exchangeRateBusiness.update(request);
    }

    @GetMapping
    public Observable<ExchangeRateEntity> list() {
        return exchangeRateBusiness.exchangeRateList();
    }

    @PostMapping(value = "/calculation", produces = {MediaType.APPLICATION_STREAM_JSON_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public Observable<ExchangeRateCalculationRs> register(@RequestBody ExchangeRateCalculationRq request) {
        return exchangeRateBusiness.calculate(request);
    }


}
