package com.arandanou.challenge.web;


import com.arandanou.challenge.model.entity.UserEntity;
import com.arandanou.challenge.model.expose.LoginRq;
import com.arandanou.challenge.model.expose.LoginRs;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class LoginController {

    @PostMapping("login")
    public LoginRs login(@RequestBody LoginRq request) {
        //TODO: query to db

        UserEntity user = UserEntity.builder()
                .id("1")
                .email("admin@leadgods.com")
                .name("ledgods")
                .password("admin")
                .build();

        String token = getJWTToken(user);
        return LoginRs.builder()
                .token(token)
                .build();
    }

    private String getJWTToken(UserEntity user) {
        String secretKey = "mySecretKey";
        List<GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER");

        String token = Jwts
                .builder()
                .setId(user.getId())//TODO: userID
                .setSubject(user.getName())
                .claim("authorities",
                        grantedAuthorities.stream()
                                .map(GrantedAuthority::getAuthority)
                                .collect(Collectors.toList())
                )
                .signWith(SignatureAlgorithm.HS512,
                        secretKey.getBytes()).compact();

        return "Bearer " + token;
    }
}
